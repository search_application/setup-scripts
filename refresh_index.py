import json
import unirest

# delete all previous messages
uri = "http://localhost:9200/chat"
res = unirest.delete(uri)
print res.body


#update mapping type
mapping = {
  "mappings": {
    "message": {
      "properties": {
          
        "date": {
          "type": "date", 
          "format": "dd/MM/yyyy:h:mm:a"
        },
        
        
        
        "body_suggest" : {   
                            "type" : "completion",
                            "analyzer" : "standard",
                            "search_analyzer" : "simple",
                            "payloads" : "false"
                        },
        
        "name_suggest" : {      
                            "type" : "completion",
                            "analyzer" : "simple",
                            "search_analyzer" : "simple",
                            "payloads" : "false"
                        }
        
        
        
      }
    }
  }
}


        
uri = "http://localhost:9200/chat"

res = unirest.put(uri, headers={ "Accept": "application/json", "Content-Type":"application/json" }, params=json.dumps(mapping))
print res.body



##############################################
#same stuff for files
# delete all previous messages
uri = "http://localhost:9200/uploads"
res = unirest.delete(uri)
print res.body


mapping = {
    "mappings" : {
        "files" : {
            "properties" : {
                "mimeType" : {
                    "type" : "string",
                    "index" : "not_analyzed" 
                },
                
            }
        }
    }

}
                


res = unirest.put(uri, headers={ "Accept": "application/json", "Content-Type":"application/json" }, params=json.dumps(mapping))
print res.body
    

